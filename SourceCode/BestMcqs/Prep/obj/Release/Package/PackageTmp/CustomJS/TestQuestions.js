﻿$(document).ready(function () {


    var questId = getUrlVars()["QuestionId"]

    if (questId != null)
        GetQuestionDataByID(questId);

    $('#ddlSubjects').change(function () {

        GetTopicBySubjectId();
    });


    $('.add-button').click(function () {

        AddOptionRow();
    });

    $('#TestQuestionNote').summernote({
        placeholder: '',
        tabsize: 2,
        height: 100
    });
    $('#CorrectAnswerDescriptionNote').summernote({
        placeholder: '',
        tabsize: 2,
        height: 100
    });

    $('#answeroption1').summernote({
        placeholder: '',
        tabsize: 2,
        height: 100
    });
});

function AddOptionRow() {

    var count = $("tr.contacts-record").length + 1;

    if (count <= 5) {

        var tblHtml = "<tr class='contacts-record' id=row" + count + "> " +
                      "  <td> " +
                      "<div> "+
                      " <div id=answeroption"+count+" data_option_externalid=" + count + "></div> "+
                      "</div> " +
                      "<div> " +
                      "      <input id='QuestionOptionFile' name='TestQuestionAnswerFile' type='file' data-option-imageid=" + count + "> " +
                      "</div> " +
                      "<div> " +
                      "      <image id='imgQuestionOptionFile' src='' data-option-imageviewerid=" + count + " style='display:none' width='500px' class='imgBorder'/> " +
                      "</div> " +
                      "      <input id='hdnQuestionOptionFile' type='hidden' value='' name='hiddenFile' data-option-hdnimageid=" + count + " /> " +
                      "  </td> " +
                      "  <td> " +
                      "      <input id='TestCorrectAnswerId' name='TestCorrectAnswerId'  type='checkbox' data-externalid=" + count + " value='true' onclick='MarkAnswerLength(this);'> <input name='TestCorrectAnswerId' type='hidden' value='false'>" +
                      "  </td> " +
                      "  <td> " +
                      " <img src='/Content/images/aaaacrosss.svg' style='width:20px;height:20px;' onclick='removerow(this);' data-option-removeid=" + count + " > " +
                      "  </td> " +
                    "</tr>";
        $('#tblOptions').append(tblHtml);

        $('#answeroption'+count).summernote({
            placeholder: '',
            tabsize: 2,
            height: 100
        });


    } else {

        alert('Mote then 5 options cannot be added');
    }
}

function GetTopicBySubjectId() {

    var selectedSubject = $('#ddlSubjects').val();
    if (selectedSubject != null && selectedSubject != '') {
        $.getJSON('/Admin/GetTopics', { subjectId: selectedSubject }, function (topics) {
            var topicsSelect = $('#ddlTopics');
            topicsSelect.empty();

            topicsSelect.append($('<option/>', {
                value: 0,
                text: 'Select'
            }));
            $.each(topics, function (index, topic) {
                topicsSelect.append($('<option/>', {
                    value: topic.TestTopicsId,
                    text: topic.TestTopicsName
                }));
            });
            $("#ddlTopics").val('0').change();
        });
    }
}

function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

function GetQuestionDataByID(questId) {

    $(document).prop('title', 'Update Questions');
    $("h2").text("Update Questions")

    var correctAnswerId = 0;
    $('#hdnSelectedAnswer').val(questId);
    $.ajaxSetup({
        async: false
    });

    $.ajax({
        url: 'GetQuestionDetailByQuestionId',
        type: 'POST',
        dataType: 'json',
        data: '{TestQuestionsId:' + questId + '}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var list = data;
            $('#hdnTestQuestionId').val(questId)
            $("#ddlSubjects").val(list[0].SelectedTestQuestionSubjectId).change();
            GetTopicBySubjectId();
            $("#ddlTopics").val(list[0].SelectedTestTopicId).change();
            $("#ddlDifficulty").val(list[0].SelectedTestDifficultyId).change();
            $("#SelectedTestQuestionTypeId").val(list[0].SelectedTestQuestionTypeId).change();
            //$("#txtQuestions").val(list[0].TestQuestion);
            $('#TestQuestionNote').summernote('code', list[0].TestQuestion);
            $('#CorrectAnswerDescriptionNote').summernote('code', list[0].CorrectAnswerDescription);
            //$("#txtAnswerDescription").val(list[0].CorrectAnswerDescription);
            correctAnswerId = list[0].TestAnswerId;

            if (list[0].TestQuestionFileName != '' && list[0].TestQuestionFileName != null) {

                $('#imgQuestionFile').show();
                $('#imgQuestionFile').attr('src', '/MCQImages/' + list[0].TestQuestionFileName);
                $('#hdnImgQuestionFile').val(list[0].TestQuestionFileName);
                $('#imgQuestionImage').show();
            }

            if (list[0].AnswerDescriptionFileName != '' && list[0].TestQuestionFileName != null) {

                $('#imgDescriptionFile').show();
                $('#imgDescriptionFile').attr('src', '/MCQImages/' + list[0].AnswerDescriptionFileName);
                $('#hdnImgDescriptionFile').val(list[0].AnswerDescriptionFileName);
                $('#imgDescriprionImage').show();
            }

            $("input[type=submit]").val('Update');
            var i = 0;
            $.each(list, function (index, item) {
                i++;
                if (i > 1)
                    AddOptionRow();

                $('#answeroption' + i).summernote('code', item.TestQuestionOption);

                if (item.TestQuestionOptionsFile != '' && item.TestQuestionOptionsFile != null) {

                    $('input[data-option-hdnimageid=' + i + ']').val(item.TestQuestionOptionsFile);
                    $('img[data-option-imageviewerid=' + i + ']').attr('src', '/MCQImages/' + item.TestQuestionOptionsFile);
                    $('img[data-option-imageviewerid=' + i + ']').css('display', '');
                }
            });
        },
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
        }
    });
    $('input[data-externalid=' + correctAnswerId + ']').prop('checked', true);
}

function MarkAnswerLength(checkbox) {

    //alert(checkbox)
    if ($('#tblOptions input[id="TestCorrectAnswerId"]:checked').length > 1) {

        alert('More then 1 option cannot be marked as Answer');
        $('input[data-externalid=' + checkbox.dataset.externalid + ']').prop('checked', false);
    }
}



function validateForm() {

    var questionFileName = '', answerDescriptionFileName = '', correctAnswerNo = 0;

    var TestQuestion = $('#TestQuestionNote').summernote('code');
    TestQuestion = TestQuestion.replace('<p><br></p>', '');

    if (TestQuestion == '' && document.getElementById("QuestionFile").files.length == 0) {

        alert('Question Image or Text should be added');
        return false;
    }
    
    if ($('#tblOptions input[id="TestCorrectAnswerId"]:checked').length == 0) {

        alert('At least one answer should be marked');
        return false;
    }


    if ($('#hdnTestQuestionId').val() != "")
        questionFileName = $('#hdnImgQuestionFile').val();

    if (document.getElementById("QuestionFile").files.length > 0) {

        questionFileName = $('#QuestionFile')[0].files[0].name;
        var questionFileSize = document.getElementById('QuestionFile').files[0].size;

        if (questionFileSize > 1048576) {//1 MB size in bytes

            alert('Question Image File size is greater than 1 MB');
            answerOptionFlag = false;
        }
    }


    if (document.getElementById("DescriptionFile").files.length > 0) {

        var answerDescriptionFileSize = document.getElementById('DescriptionFile').files[0].size;

        if (answerDescriptionFileSize > 1048576) {//1 MB size in bytes

            alert('Answer Description Image File size is greater than 1 MB');
            answerOptionFlag = false;
        }
    }


    var QuestionFileUpload = $("#QuestionFile").get(0);
    var Questionfile = QuestionFileUpload.files;

    var i = 0;
    var answerOptionFlag = true;
    var testansweroptions = '';

    $("#tblOptions tr").each(function () {


        if (i > 0) {


            var textval = $('#answeroption' + i).summernote('code');
            var fileval = $(this).find("td").eq(0).find(":file")[1].files.length;
            var filepath = $(this).find("td").eq(0).find(":hidden")[419].value;
            

            textval = textval.replace('<p><br></p>','');

            var fileSize = 0, fileName = "";


            if ($('#hdnTestQuestionId').val() != "" && (typeof filepath !== "undefined"))
                fileName = filepath;

            if (fileval > 0) {

                fileSize = $(this).find("td").eq(0).find(":file")[1].files[0].size;
                fileName = $(this).find("td").eq(0).find(":file")[1].files[0].name;


                var formData = new FormData();
                formData.append('file', $(this).find("td").eq(0).find(":file")[1].files[0]);
                
                $.ajax({
                    url: 'UploadImage',
                    type: 'POST',
                    data: formData,
                    processData: false,  // tell jQuery not to process the data
                    contentType: false,  // tell jQuery not to set contentType
                    async: false,
                    success: function (data) {
                        console.log(data);
                    }
                });
            }

            if (textval == '' && fileval == 0 && filepath == '') {

                alert('Option Image or Text should be added');
                answerOptionFlag = false;
            }

            if (fileSize > 1048576) {//1 MB size in bytes

                alert('Option Image File size is greater than 1 MB');
                answerOptionFlag = false;
            }

            if ($(this).find("td").eq(1).find(":checkbox")[0].checked)
                correctAnswerNo = i;

            testansweroptions += i + "~" + textval + "~" + fileName + "|";
        }
        i++;
    })

    if (answerOptionFlag) {
        
        //var requestData = {
        //    TestQuestionId: $('#hdnTestQuestionId').val(),
        //    SubjectId: $('#ddlSubjects').val(),
        //    TopicId: $('#ddlTopics').val(),
        //    DifficultyId: $('#ddlDifficulty').val(),
        //    TestQuestion: $('#TestQuestionNote').summernote('code'),
        //    CorrectAnswerDescription: $('#CorrectAnswerDescriptionNote').summernote('code'),
        //    TestAnswerOptions: testansweroptions,
        //    QuestionFile: Questionfile
        //};
        
        var file = $("#QuestionFile").val();

        if (file.length > 0) {

        var formData = new FormData();
        formData.append('file', $("#QuestionFile")[0].files[0]);


            $.ajax({
                url: 'UploadImage',
                type: 'POST',
                data: formData,
                processData: false,  // tell jQuery not to process the data
                contentType: false,  // tell jQuery not to set contentType
                async:false,
                success: function (data) {
                    console.log(data);
                }
            });
        }

        var descriptionFile = $('#DescriptionFile').val();


        if ($('#hdnTestQuestionId').val() != "")
            answerDescriptionFileName = $('#hdnImgDescriptionFile').val();
        
        if (descriptionFile.length > 0) {

            answerDescriptionFileName = $("#DescriptionFile")[0].files[0].name;
            var formData = new FormData();
            formData.append('file', $("#DescriptionFile")[0].files[0]);

            $.ajax({
                url: 'UploadImage',
                type: 'POST',
                data: formData,
                processData: false,  // tell jQuery not to process the data
                contentType: false,  // tell jQuery not to set contentType
                async: false,
                success: function (data) {
                    console.log(data);
                }
            });
        }
        //var frmValues = $('#AddTestQuestion').serialize();
        $.ajax({
            type: 'POST',
            url: 'AddTestQuestions',
            async: false,
            data: {
                TestQuestionId: $('#hdnTestQuestionId').val(),
                SubjectId: $('#ddlSubjects').val(),
                TopicId: $('#ddlTopics').val(),
                DifficultyId: $('#ddlDifficulty').val(),
                TestQuestion: $('#TestQuestionNote').summernote('code'),
                CorrectAnswerDescription: $('#CorrectAnswerDescriptionNote').summernote('code'),
                TestAnswerOptions: testansweroptions,
                TestQuestionFile: questionFileName,
                AnswerDescriptionFile: answerDescriptionFileName,
                correctAnswerId: correctAnswerNo
            }
        })
        .done(function () {
            if ($('#hdnTestQuestionId').val() != '') {

                alert('Record updated successfully');
                window.location.href = "GetQuestions";
            }
            else {

                alert('Record added successfully');
                window.location.href = "GetQuestions";
            }
        })
        .fail(function () {
            $("#para").text("An error occured");
        });
    }
}

function removerow(imgId) {

    var rowId = imgId.dataset.optionRemoveid;
    $('#row' + rowId).remove();

    var i = 1;

    $('#tblOptions .contacts-record').each(function () {

        if (i >= parseInt(rowId)) {

            //document.getElementById("TestQuestionOptions").setAttribute("data-option-externalid", i);
            this.id = "row" + i;
            var optionText = $(this).find('td div')[1];
            //optionText.attributes.data_option_externalid = i;
            optionText.attributes[1].value = i;
            optionText.attributes[0].value = "answeroption" + i;
            var optionValue = $(this).find('td input')[6];
            optionValue.dataset.optionImageid = i;
            var optionValue = $(this).find('td input')[7];
            optionValue.dataset.optionHdnimageid = i;
            var optionValue = $(this).find('td input')[8];
            optionValue.dataset.externalid = i;
            var optionValue = $(this).find('td img')[0];
            optionValue.dataset.optionImageviewerid = i;
            var crossimg = $(this).find('td img')[1];
            crossimg.dataset.optionRemoveid = i
        }
        i++;
    });
}

function removequestionimage() {

    $('#imgQuestionFile').attr('src', '');
    $('#imgQuestionFile').hide();
    $('#imgQuestionImage').hide();
    $('#hdnImgQuestionFile').val('');
}

function removedescriptionimage() {

    $('#imgDescriptionFile').attr('src', '');
    $('#imgDescriptionFile').hide();
    $('#imgDescriprionImage').hide();
    $('#hdnImgDescriptionFile').val('');
}