﻿using Prep.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using System.Collections;
using System.Net.Http.Formatting;

namespace Prep.Controllers
{
    public class AdminController : Controller
    {
        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }


        [AllowAnonymous]
        [HttpPost]
        public ActionResult Login(VMLogin objLogin)
        {
            if (objLogin.LoginUserId.ToString().ToLower() == "admin" && objLogin.LoginPassword.ToString().ToLower() == "1234")
            {
                ViewBag.LoginMessage = "User Login Successfully";
                Session["LoginUserId"] = objLogin.LoginUserId;
                return RedirectToAction("Index");
            }
            else
            {
                ViewBag.LoginMessage = "Incorrect Credentials for Login";
                return View("Login");
            }
        }

        //[CustomAuthentication]
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult AddTestQuestions()
        {
            SPs objSPs = new SPs();
            VMTestQuestions TestQuestionVM = new VMTestQuestions();

            TestQuestionVM.TestQuestionSubjects = objSPs.GetActiveSubjects();

            TestQuestionVM.TestTopic = new List<TestTopics>
            {
                //    new TestTopics{ TestTopicsId=1, TestTopicsName="A" },
                //    new TestTopics{ TestTopicsId=2, TestTopicsName="B" },
                //    new TestTopics{ TestTopicsId=3, TestTopicsName="C" }
            };


            TestQuestionVM.TestDifficulty = objSPs.GetActiveQuestionDifficulty();


            //TestQuestionVM.TestQuestionTypeId = objSPs.GetActiveQuestionType();

            return View(TestQuestionVM);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddTestQuestions(string TestQuestionId, string SubjectId, string TopicId, string DifficultyId, string TestQuestion, string CorrectAnswerDescription, string TestAnswerOptions, string TestQuestionFile, string AnswerDescriptionFile, int? correctAnswerId)
        {
            if (TestQuestionId != null && SubjectId != null && TopicId != null && DifficultyId != null && TestQuestion != null && CorrectAnswerDescription != null && TestAnswerOptions != null && TestQuestionFile != null && AnswerDescriptionFile != null)
            {
                bool ValidationError = false;
                //var hiddenValues = fc["hiddens"].Split(',');
                var hdnTestQuestionId = TestQuestionId;


                //var hiddenImageFiles = fc["hiddenFile"].Split(',');


                SPs objSPs = new SPs();
                VMTestQuestions TestQuestionVM = new VMTestQuestions();

                DataTable dt = objSPs.CreateAnswersTable();
                var letters = TestAnswerOptions.Split('|'); //yields an array containing { "A", "B", "C" }

                for (var i = 0; i < letters.Length - 1; i++)
                {
                    var answerOptions = letters[i].Split('~');
                    dt.Rows.Add(answerOptions[0], answerOptions[1], answerOptions[2]);
                }

                //int correctAnswerId = 0;
                //var arrAnswerId = fc["TestCorrectAnswerId"].Split(',');
                //var j = 0;
                ////Iterate through each of the letters
                //foreach (var answerId in arrAnswerId)
                //{
                //    j++;
                //    if (answerId == "true")
                //        correctAnswerId = j;
                //}

                Boolean checkQuestionAdded = false;

                //if (hdnTestQuestionId == "")
                //{
                //    if (string.IsNullOrEmpty(objVMTestQuestions.TestQuestion) && objVMTestQuestions.TestQuestionFile == null)
                //    {
                //        ValidationError = true;
                //        TempData["Message"] = string.Format("Please provide Test Question or its image.");
                //    }


                //    //if (string.IsNullOrEmpty(objVMTestQuestions.CorrectAnswerDescription) && objVMTestQuestions.AnswerDescriptionFile == null)
                //    //{
                //    //    ValidationError = true;
                //    //    TempData["Message"] = string.Format("Please provide Answer Description or its image.");
                //    //}
                //}
                //else
                //{
                //    if (string.IsNullOrEmpty(objVMTestQuestions.TestQuestion) && objVMTestQuestions.TestQuestionFile == null && hiddenValues[2] ==null)
                //    {
                //        ValidationError = true;
                //        TempData["Message"] = string.Format("Please provide Test Question or its image.");
                //    }


                //    //if (string.IsNullOrEmpty(objVMTestQuestions.CorrectAnswerDescription) && objVMTestQuestions.AnswerDescriptionFile == null && hiddenValues[3] == null)
                //    //{
                //    //    ValidationError = true;
                //    //    TempData["Message"] = string.Format("Please provide Answer Description or its image.");
                //    //}
                //}


                //if (ModelState.ContainsKey("TestQuestionOptions") && correctAnswerId > 0)
                //    ModelState["TestQuestionOptions"].Errors.Clear();

                //if (ModelState.ContainsKey("TestQuestionSubjects"))
                //    ModelState["TestQuestionSubjects"].Errors.Clear();

                //if (ModelState.ContainsKey("TestTopic"))
                //    ModelState["TestTopic"].Errors.Clear();

                //if (ModelState.ContainsKey("TestDifficulty"))
                //    ModelState["TestDifficulty"].Errors.Clear();

                ////if (ModelState.ContainsKey("TestQuestionTypeId"))
                ////    ModelState["TestQuestionTypeId"].Errors.Clear();

                //if (ModelState.IsValid && !ValidationError)
                //{
                    if (hdnTestQuestionId != "")
                        checkQuestionAdded = objSPs.UpdateQuestions(Convert.ToInt32(hdnTestQuestionId), Convert.ToInt32(SubjectId), Convert.ToInt32(TopicId), 1, Convert.ToInt32(DifficultyId), TestQuestion, TestQuestionFile, Convert.ToInt32(correctAnswerId), dt, CorrectAnswerDescription, AnswerDescriptionFile);
                    else
                        checkQuestionAdded = objSPs.AddQuestions(Convert.ToInt32(SubjectId), Convert.ToInt32(TopicId), 1, Convert.ToInt32(DifficultyId), TestQuestion, TestQuestionFile, Convert.ToInt32(correctAnswerId), dt, CorrectAnswerDescription, AnswerDescriptionFile);
                //}

                //if (objVMTestQuestions.TestQuestionFile != null)
                //{
                //    objVMTestQuestions.TestQuestionFile.SaveAs(Server.MapPath("~/MCQImages/" + objVMTestQuestions.TestQuestionFile.FileName));
                //}
                //if (objVMTestQuestions.AnswerDescriptionFile != null)
                //{
                //    objVMTestQuestions.AnswerDescriptionFile.SaveAs(Server.MapPath("~/MCQImages/" + objVMTestQuestions.AnswerDescriptionFile.FileName));
                //}
                TestQuestionVM.TestQuestionSubjects = objSPs.GetActiveSubjects();

                TestQuestionVM.TestTopic = new List<TestTopics>
                {
                    //    new TestTopics{ TestTopicsId=1, TestTopicsName="A" },
                    //    new TestTopics{ TestTopicsId=2, TestTopicsName="B" },
                    //    new TestTopics{ TestTopicsId=3, TestTopicsName="C" }
                };


                TestQuestionVM.TestDifficulty = objSPs.GetActiveQuestionDifficulty();


                //TestQuestionVM.TestQuestionTypeId = objSPs.GetActiveQuestionType();

                if (checkQuestionAdded)
                {
                    TempData["Message"] = string.Format("Record added successfully");
                    ViewBag.RecordUpdated = 1;
                }
                else
                {
                    TempData["Message"] = string.Format("Some error occured");
                    ViewBag.RecordUpdated = 0;
                }
                if (hdnTestQuestionId != "")
                    return RedirectToAction("AddTestQuestions", new { QuestionId = hdnTestQuestionId });
                else
                    return RedirectToAction("AddTestQuestions");
            }
            else
                return RedirectToAction("AddTestQuestions");
        }
        // GET: Admin
        public ActionResult GetTopics(int subjectId)
        {
            SPs objSPs = new SPs();
            return Json(objSPs.GetActiveSubjectsByTopicId(subjectId), JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetQuestions(TestQuestionSearchModel objTQSearchModel, string search ="", int page = 1)
        {
            string searchType = "";
            SPs objSPs = new SPs();
            TestQuestionSearchModel objTQVM = new TestQuestionSearchModel();
            objTQVM.TestQuestionSubjects = objSPs.GetActiveSubjects();
            objTQVM.TestTopic = new List<TestTopics>
            {
            };
            objTQVM.TestDifficulty = objSPs.GetActiveQuestionDifficulty();
            TempData["VMTestQuestions"] = objTQVM;

            //remove session variable of search perameters
            //Session.Remove("SearchParameters");

            ArrayList arrSearch = new ArrayList();

            arrSearch.Add(objTQSearchModel.SelectedTestQuestionSubjectId);
            arrSearch.Add(objTQSearchModel.SelectedTestTopicId);
            arrSearch.Add(objTQSearchModel.SelectedTestDifficultyId);
            arrSearch.Add(search);
            //if (Session["SearchParameters"] == null)
            //{

            arrSearch.Add(objTQSearchModel.SelectedTestQuestionSubjectId);
            arrSearch.Add(objTQSearchModel.SelectedTestTopicId);
            arrSearch.Add(objTQSearchModel.SelectedTestDifficultyId);
            arrSearch.Add(search);
            arrSearch.Add(searchType);
            

            ViewBag.ddSubject = objTQSearchModel.SelectedTestQuestionSubjectId;
            ViewBag.ddTopic = objTQSearchModel.SelectedTestTopicId;
            ViewBag.ddDifficulty = objTQSearchModel.SelectedTestTopicId;
            ViewBag.questionSearch = search;

            return View(this.GetTestQuestions(page, 10, arrSearch));

            var SelectedTestQuestionSubjectId = ViewBag.ddSubject;// objTQSearchModel.SelectedTestQuestionSubjectId;
            var SelectedTestTopicId = ViewBag.ddTopic;// objTQSearchModel.SelectedTestTopicId;
            
            var SelectedTestDifficultyId = ViewBag.ddDifficulty; //objTQSearchModel.SelectedTestDifficultyId;

                arrSearch.Add(search);
                arrSearch.Add(searchType);
                arrSearch.Add(SelectedTestQuestionSubjectId);
                arrSearch.Add(SelectedTestTopicId);
                arrSearch.Add(SelectedTestDifficultyId);

                ViewBag.SearchParamList = arrSearch;

            //}
            //else
            //{
            //}
            ArrayList arrSearch1 = (ArrayList)ViewBag.SearchParamList;



            return View(this.GetTestQuestions(page, 10, arrSearch1,searchType));
            return View(this.GetTestQuestions(page, 10, arrSearch, searchType));

        }

        

        [HttpPost]

        private TestQuestionModel GetTestQuestions(int page, int pageSize, ArrayList arrSearch, string searchType = "")
        {
            SPs objSPs = new SPs();
            TestQuestionModel objTestQuestionModel = new TestQuestionModel();
            objTestQuestionModel.CurrentPageIndex = page;
            objTestQuestionModel.PageCount = pageSize;
            objTestQuestionModel.TestQuestions = objSPs.GetListTestQuestions(pageSize, page, arrSearch);
            ViewBag.PageCount = ((objTestQuestionModel.TestQuestions.Count > 0) ? objTestQuestionModel.TestQuestions[0].TotalRecordCount : 0);
            ViewBag.PageSize = pageSize;
            Double totalpages = 0;
            if (ViewBag.PageCount > 0)
                totalpages = Convert.ToDouble(ViewBag.PageCount) / Convert.ToDouble(ViewBag.PageSize);
            ViewBag.totalpages = Math.Ceiling(totalpages);
            return objTestQuestionModel;
        }

        public ActionResult DeleteQuestions(int TestQuestionId)
        {
            SPs objSPs = new SPs();
            bool isDeleted = objSPs.DeleteTestQuestions(TestQuestionId);
            return RedirectToAction("GetQuestions"); //View("GetTestQuestions");
        }


        public JsonResult GetQuestionDetailByQuestionId(int TestQuestionsId)
        {
            SPs objSPs = new SPs();
            var QuestionDetail = objSPs.GetTestQuestionById(TestQuestionsId);
            //return Json(QuestionDetail, JsonRequestBehavior.AllowGet);
            return Json(QuestionDetail.Tables[0].AsEnumerable()
                .Select(dr => new VMTestQuestions
                {
                    SelectedTestQuestionSubjectId = dr.Field<int>("TestQuestionsSubjectId")
                    ,
                    SelectedTestTopicId = dr.Field<int>("TestTopicsId")
                    ,
                    SelectedTestQuestionTypeId = dr.Field<int>("TestQuestionId")
                    ,
                    SelectedTestDifficultyId = dr.Field<int>("TestDifficultyId")
                    ,
                    TestQuestion = dr.Field<string>("TestQuestion")
                    ,
                    TestAnswerId = dr.Field<int>("TestCorrectAnswerId")
                    ,
                    TestQuestionOption = dr.Field<string>("TestQuestionAnswer")
                    ,
                    CorrectAnswerDescription = dr.Field<string>("CorrectAnswerDescription")
                    ,
                    TestQuestionFileName = dr.Field<string>("TestQuestionImage")
                    ,
                    AnswerDescriptionFileName = dr.Field<string>("CorrectAnswerDescriptionImage")
                    ,
                    TestQuestionOptionsFile = dr.Field<string>("TestAnswerImage")
                }), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetSubjects()
        {

            SPs objSPs = new SPs();
            List<TestQuestionsSubjects> objTestQuestionsSubjects = objSPs.GetActiveSubjects();
            return View(objTestQuestionsSubjects);
        }

        public ActionResult DeleteSubject(int TestQuestionsSubjectId)
        {
            SPs objSPs = new SPs();
            bool isDeleted = objSPs.DeleteSubject(TestQuestionsSubjectId);
            return RedirectToAction("GetSubjects"); //View("GetTestQuestions");
        }


        public ActionResult AddSubjects()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddSubjects(TestQuestionsSubjects objTestQuestionsSubjects, FormCollection fc)
        {
            TempData["RecordUpdated"] = 0;
            SPs objSPs = new SPs();
            var hiddenValues = fc["hiddens"].Split(',');
            var hdnSubjectId = hiddenValues[0];

            if (objTestQuestionsSubjects.TestQuestionsSubject == null)
            {
                TempData["Message"] = string.Format("Please enter Subject");
                TempData["RecordUpdated"] = 1;
            }
            
            DataSet dsSubject = objSPs.GetSubjectByName(objTestQuestionsSubjects.TestQuestionsSubject, 0);

            if (dsSubject.Tables[0].Rows.Count > 0)
            {

                TempData["Message"] = string.Format("Subject already exist");
                TempData["RecordUpdated"] = 2;
            }

            bool isAdded = false;
            if (Convert.ToInt32(TempData["RecordUpdated"]) == 0)
            {
                if (hdnSubjectId != "")
                    isAdded = objSPs.UpdateSubjects(Convert.ToInt32(hdnSubjectId), objTestQuestionsSubjects.TestQuestionsSubject);
                else
                    isAdded = objSPs.AddSubjects(objTestQuestionsSubjects.TestQuestionsSubject);
            }

            if (isAdded)
            {
                TempData["Message"] = string.Format("Record added successfully");
                TempData["RecordUpdated"] = 1;
            }
            
            if (hdnSubjectId != "")
                return RedirectToAction("AddSubjects", new { SubjectId = hdnSubjectId });
            else
                return RedirectToAction("AddSubjects");

            //return View(viewname);
        }
        public ActionResult GetSubjectBySubjectId(int SubjectId)
        {
            SPs objSPs = new SPs();
            var SubjectDetail = objSPs.GetSubjectByName("", SubjectId);
            return Json(SubjectDetail.Tables[0].AsEnumerable()
                .Select(dr => new TestQuestionsSubjects
                {
                    TestQuestionsSubject = dr.Field<string>("SubjectName")
                    ,
                    TestQuestionsSubjectId = dr.Field<int>("SubjectId")
                }), JsonRequestBehavior.AllowGet);
        }
        
        public ActionResult GetDifficultyLevel()
        {

            SPs objSPs = new SPs();
            List<TestDifficulties> objTestDifficulties = objSPs.GetActiveQuestionDifficulty();
            return View(objTestDifficulties);
        }


        public ActionResult DeleteDifficultyLevel(int TestDifficultyId)
        {
            SPs objSPs = new SPs();
            bool isDeleted = objSPs.DeleteDifficultyLevel(TestDifficultyId);
            return RedirectToAction("GetDifficultyLevel"); //View("GetTestQuestions");
        }
        
        public ActionResult AddDifficultyLevel()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddDifficultyLevel(TestDifficulties objTestDifficulties, FormCollection fc)
        {
            TempData["RecordUpdated"] = 0;
            SPs objSPs = new SPs();
            var hiddenValues = fc["hiddens"].Split(',');
            var hdnDiffLevelId = hiddenValues[0];

            if (objTestDifficulties.TestDifficultyName == null)
            {
                TempData["Message"] = string.Format("Please enter Difficulty Level");
                TempData["RecordUpdated"] = 1;
            }

            DataSet dsSubject = objSPs.GetDiffLevelByName(objTestDifficulties.TestDifficultyName, 0);

            if (dsSubject.Tables[0].Rows.Count > 0)
            {

                TempData["Message"] = string.Format("Difficulty Level already exist");
                TempData["RecordUpdated"] = 2;
            }

            bool isAdded = false;
            if (Convert.ToInt32(TempData["RecordUpdated"]) == 0)
            {
                if (hdnDiffLevelId != "")
                    isAdded = objSPs.UpdateDifficultyLevel(Convert.ToInt32(hdnDiffLevelId), objTestDifficulties.TestDifficultyName);
                else
                    isAdded = objSPs.AddDifficultyLevel(objTestDifficulties.TestDifficultyName);
            }

            if (isAdded)
            {
                TempData["Message"] = string.Format("Record added successfully");
                TempData["RecordUpdated"] = 1;
            }

            if (hdnDiffLevelId != "")
                return RedirectToAction("AddDifficultyLevel", new { DifficultyId = hdnDiffLevelId });
            else
                return RedirectToAction("AddDifficultyLevel");

            //return View(viewname);
        }

        public ActionResult GetDiffLevelByDiffLevelId(int DiffLevelId)
        {
            SPs objSPs = new SPs();
            var DiffLevelDetail = objSPs.GetDiffLevelByName("", DiffLevelId);
            return Json(DiffLevelDetail.Tables[0].AsEnumerable()
                .Select(dr => new TestDifficulties
                {
                    TestDifficultyId = dr.Field<int>("DiffLevelId")
                    ,
                    TestDifficultyName = dr.Field<string>("DiffLevelValue")
                }), JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetSubjectTopics()
        {
            SPs objSPs = new SPs();
            List<TestSubjectToic> objTestTopicsList = objSPs.GetActiveTopicList();
            return View(objTestTopicsList);
        }


        public ActionResult DeleteTopic(int TopicId)
        {
            SPs objSPs = new SPs();
            bool isDeleted = objSPs.DeleteTopic(TopicId);
            return RedirectToAction("GetSubjectTopics");
        }


        public ActionResult AddTopics()
        {

            SPs objSPs = new SPs();
            VMTestTopics TestTopicsVM = new VMTestTopics();

            TestTopicsVM.TestQuestionSubjects = objSPs.GetActiveSubjects();

            return View(TestTopicsVM);
        }

        [HttpPost]
        public ActionResult AddTopics(VMTestTopics objVMTestTopics, FormCollection fc)
        {
            TempData["RecordUpdated"] = 0;
            SPs objSPs = new SPs();
            var hiddenValues = fc["hiddens"].Split(',');
            var hdnTopicId = hiddenValues[0];

            if (objVMTestTopics.SelectedTestQuestionSubjectId == 0)
            {
                TempData["Message"] = string.Format("Please select subject");
                TempData["RecordUpdated"] = 2;
            }
            if(objVMTestTopics.TopicName == null)
            {
                TempData["Message"] = string.Format("Please enter Topic");
                TempData["RecordUpdated"] = 1;
            }

            if (objVMTestTopics.TopicName != null)
            {
                DataSet dsSubject = objSPs.GetTopicByName(objVMTestTopics.TopicName, 0);

                if (dsSubject.Tables[0].Rows.Count > 0)
                {

                    TempData["Message"] = string.Format("Topic already exist");
                    TempData["RecordUpdated"] = 3;
                }
            }

            bool isAdded = false;
            if (Convert.ToInt32(TempData["RecordUpdated"]) == 0)
            {
                if (hdnTopicId != "")
                    isAdded = objSPs.UpdateTopics(Convert.ToInt32(hdnTopicId), objVMTestTopics.SelectedTestQuestionSubjectId, objVMTestTopics.TopicName);
                else
                    isAdded = objSPs.AddTopics(objVMTestTopics.SelectedTestQuestionSubjectId, objVMTestTopics.TopicName);
            }

            if (isAdded)
            {
                TempData["Message"] = string.Format("Record added successfully");
                TempData["RecordUpdated"] = 1;
            }

            if (hdnTopicId != "")
                return RedirectToAction("AddTopics", new { TopicId = hdnTopicId });
            else
                return RedirectToAction("AddTopics");
        }

        public ActionResult GetTopicByTopicId(int TopicId)
        {
            SPs objSPs = new SPs();
            var TopicDetail = objSPs.GetTopicByName("", TopicId);
            return Json(TopicDetail.Tables[0].AsEnumerable()
                .Select(dr => new VMTestTopics
                {
                    
                    TopicName = dr.Field<string>("TopicName")
                    ,
                    SelectedTestQuestionSubjectId = dr.Field<int>("SubjectId")
                }), JsonRequestBehavior.AllowGet);
        }


        public string UploadImage()
        {
            int iUploadedCnt = 0;

            // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES.
            string sPath = "";
            sPath = System.Web.Hosting.HostingEnvironment.MapPath("~/MCQImages/");

            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;

            // CHECK THE FILE COUNT.
            for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];

                if (hpf.ContentLength > 0)
                {
                    // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                    //if (!File.Exists(sPath + Path.GetFileName(hpf.FileName)))
                    //{
                        // SAVE THE FILES IN THE FOLDER.
                        hpf.SaveAs(sPath + hpf.FileName);
                        iUploadedCnt = iUploadedCnt + 1;
                    //}
                }
            }

            // RETURN A MESSAGE.
            if (iUploadedCnt > 0)
            {
                return iUploadedCnt + " Files Uploaded Successfully";
            }
            else
            {
                return "Upload Failed";
            }

        }
    }
}
