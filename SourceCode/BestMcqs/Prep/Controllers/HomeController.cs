﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Prep.Controllers
{
    public class HomeController : Controller
    {
        [AllowAnonymous]
        public ActionResult Index()
        {
            ViewBag.Title = "Entry Test Preparation MCQs for Medical and Engineering colleges - PakPrep - Home";

            return View();
        }

        [AllowAnonymous]
        public ActionResult prepare()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult faqs()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult testimonials()
        {
            return View();
        }
        
        [AllowAnonymous]
        public ActionResult pastpapers()
        {
            return View();
        }
        
        [AllowAnonymous]
        public ActionResult pastpapersmed()
        {
            return View();
        }
        
        [AllowAnonymous]
        public ActionResult contactus()
        {
            return View();
        }
        
        [AllowAnonymous]
        public ActionResult paperdemo()
        {
            return View();
        }
    }
}
