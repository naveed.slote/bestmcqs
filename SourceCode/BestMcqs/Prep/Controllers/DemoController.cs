﻿using Prep.Models;
using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Prep.Controllers
{
    public class DemoController : Controller
    {
        // GET: Demo
        public ActionResult Index()
        {
            return View();
        }
        [AllowAnonymous]
        public ActionResult DemoTest(int demo)
        {
            if(demo ==1)
            {
                ViewBag.DemoTestTitle = "Sample Physics MCQs";
            }
            else if(demo == 2)
            {
                ViewBag.DemoTestTitle = "Sample Chemistry MCQs";

            }
            else if (demo == 3)
            {
                ViewBag.DemoTestTitle = "Sample Biology MCQs";

            }
            else if (demo == 4)
            {
                ViewBag.DemoTestTitle = "Sample Maths MCQs";

            }
            else if (demo == 5)
            {
                ViewBag.DemoTestTitle = "Timed Nust Mock MCQs";

            }
            else if (demo == 6)
            {
                ViewBag.DemoTestTitle = "Timed Punjab MCAT Mock";

            }
            else if (demo == 7)
            {
                ViewBag.DemoTestTitle = "UHS MCAT Past Papers";

            }
            else if (demo == 8)
            {
                ViewBag.DemoTestTitle = "ECAT Past Papers";

            }
            else if (demo == 9)
            {
                ViewBag.DemoTestTitle = "NUST Past Papers";

            }
            else if (demo == 10)
            {
                ViewBag.DemoTestTitle = "GIKI Past Papers";

            }
            else if (demo == 11)
            {
                ViewBag.DemoTestTitle = "ETEA Past Papers";

            }
            else if (demo == 12)
            {
                ViewBag.DemoTestTitle = "NTS Past Papers";

            }
            else if (demo == 13)
            {
                ViewBag.DemoTestTitle = "PAF Past Papers";

            }
            else if (demo == 14)
            {
                ViewBag.DemoTestTitle = "TCC Past Papers";

            }
            else if (demo == 15)
            {
                ViewBag.DemoTestTitle = "PIEAS Past Papers";

            }
            else if (demo == 16)
            {
                ViewBag.DemoTestTitle = "AMC Past Papers";

            }
            else if (demo == 17)
            {
                ViewBag.DemoTestTitle = "FMDC Past Papers";

            }
            return View();
        }


        [AllowAnonymous]
        public ActionResult StartDemoTest(int demo)
        {
            TempData["QuestionNo"] = 1;
            SPs objSPs = new SPs();
            //DemoTest objDemoTest = new DemoTest();
            var demoTestDetail = objSPs.GetQuestionBySubjectId(demo);
            var demoTestList = demoTestDetail.Tables[0].AsEnumerable()
                .Select(dr => new DemoTest
                {
                    RowNo = dr.Field<Int64>("RowNo")
                    ,
                    TestQuestionsId = dr.Field<int>("TestQuestionsId")
                    ,
                    TestQuestionsSubjectId = dr.Field<int>("TestQuestionsSubjectId")
                    ,
                    TestTopicsId = dr.Field<int>("TestTopicsId")
                    ,
                    TestQuestion = dr.Field<string>("TestQuestion")
                    ,
                    TestQuestionSeqId = dr.Field<int>("TestQuestionSeqId")
                    ,
                    TestQuestionAnswer = dr.Field<string>("TestQuestionAnswer")
                    ,
                    TestAnswerImage = ((dr.Field<string>("TestAnswerImage")==null)?"": dr.Field<string>("TestAnswerImage"))
                    ,
                    CorrectAnswerDescription = dr.Field<string>("CorrectAnswerDescription")
                    ,
                    TestQuestionImage = ((dr.Field<string>("TestQuestionImage") == null) ? "" : dr.Field<string>("TestQuestionImage"))
                    ,
                    CorrectAnswerDescriptionImage = ((dr.Field<string>("CorrectAnswerDescriptionImage") == null) ? "" : dr.Field<string>("CorrectAnswerDescriptionImage"))
                }).Where(a => a.RowNo == 1).ToList();
            return View(demoTestList);
        }


        [AllowAnonymous]
        [HttpPost]
        public ActionResult StartDemoTest()
        {            
            return View();
        }
    }
}