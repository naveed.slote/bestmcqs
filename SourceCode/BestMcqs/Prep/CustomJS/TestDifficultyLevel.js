﻿$(document).ready(function () {


    var DiffLevelId = getUrlVars()["DifficultyId"]

    if (DiffLevelId != null)
        GetDiffLevelByDiffLevelId(DiffLevelId);

});
function validateForm() {

    if ($('#TestDifficultyName').val() == '') {

        alert("Please enter Difficulty Level");
        return false;
    }
}

function GetDiffLevelByDiffLevelId(DiffLevelId) {

    $(document).prop('title', 'Update Difficulty Level');
    $("h2").text("Update Difficulty Level")

    $.ajax({
        url: 'GetDiffLevelByDiffLevelId?DiffLevelId=' + DiffLevelId,
        type: 'GET',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var list = data;
            $('#hdnDiffLevelId').val(list[0].TestDifficultyId);
            $('#TestDifficultyName').val(list[0].TestDifficultyName);
            $("input[type=submit]").val('Update');
        },
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
        }
    });
}

function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}
