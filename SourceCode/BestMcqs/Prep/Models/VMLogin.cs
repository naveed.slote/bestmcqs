﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Prep.Models
{
    public class VMLogin
    {
        public string LoginUserId { get; set; }
        public string LoginPassword { get; set; }
    }
}