﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Prep.Models
{
    public class SPs
    {
        public List<TestQuestionsSubjects> GetActiveSubjects()
        {
            DataSet ds = new DataSet();
            string connstring = ConfigurationManager.ConnectionStrings["PrepConnectionString"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connstring))
            {
                SqlCommand sqlComm = new SqlCommand("spGetActiveSubjects", conn);

                sqlComm.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = sqlComm;

                da.Fill(ds);
                var questionSubjectList = ds.Tables[0].AsEnumerable()
                    .Select(dataRow => new TestQuestionsSubjects 
                    { TestQuestionsSubjectId = dataRow.Field<int>("SubjectId"),
                        TestQuestionsSubject = dataRow.Field<string>("SubjectName") ,
                        TestSubQuestionCount = dataRow.Field<int>("subQuestionCount")
                    }).ToList();
                return questionSubjectList;
            }
        }


        public List<TestTopics> GetActiveSubjectsByTopicId(int SubjectId)
        {
            DataSet ds = new DataSet();
            string connstring = ConfigurationManager.ConnectionStrings["PrepConnectionString"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connstring))
            {
                SqlCommand sqlComm = new SqlCommand("spGetActiveTopicBySubjectId", conn);
                sqlComm.Parameters.Add(new SqlParameter("@SubjectId", SubjectId));
                sqlComm.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = sqlComm;

                da.Fill(ds);
                var questionTopicList = ds.Tables[0].AsEnumerable().Select(dataRow => new TestTopics { TestTopicsId = dataRow.Field<int>("TopicId"), TestTopicsName = dataRow.Field<string>("TopicName") }).ToList();
                return questionTopicList;
            }
        }


        public List<TestQuestionTypes> GetActiveQuestionType()
        {
            DataSet ds = new DataSet();
            string connstring = ConfigurationManager.ConnectionStrings["PrepConnectionString"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connstring))
            {
                SqlCommand sqlComm = new SqlCommand("spGetActiveQuestionType", conn);

                sqlComm.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = sqlComm;

                da.Fill(ds);
                var questionTypeList = ds.Tables[0].AsEnumerable().Select(dataRow => new TestQuestionTypes { TestQuestionTypeId = dataRow.Field<int>("QuestionTypeId"), TestQuestionTypeName = dataRow.Field<string>("QuestionType") }).ToList();
                return questionTypeList;
            }
        }


        public List<TestDifficulties> GetActiveQuestionDifficulty()
        {
            DataSet ds = new DataSet();
            string connstring = ConfigurationManager.ConnectionStrings["PrepConnectionString"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connstring))
            {
                SqlCommand sqlComm = new SqlCommand("spGetActiveDifficultyLevel", conn);

                sqlComm.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = sqlComm;

                da.Fill(ds);
                var questionDifficultyList = ds.Tables[0].AsEnumerable().Select(dataRow => new TestDifficulties { TestDifficultyId = dataRow.Field<int>("DiffLevelId"), TestDifficultyName = dataRow.Field<string>("DiffLevelValue") }).ToList();
                return questionDifficultyList;
            }
        }

        public bool AddQuestions(int subjectId, int topicId, int questionId, int difficultyId, string question, string questionImagepath, int correctAnswerId, DataTable answers, string CorrectAnswerDescription, string CorrectAnswerDescriptionpath)
        {
            string connstring = ConfigurationManager.ConnectionStrings["PrepConnectionString"].ConnectionString;
            try
            {
                using (SqlConnection conn = new SqlConnection(connstring))
                {
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandText = "spAddQuestions"; //The name of the other stored procedure.  
                    cmd.Parameters.AddWithValue("@TestQuestionsSubjectId", subjectId); 
                    cmd.Parameters.AddWithValue("@TestTopicsId", topicId);
                    cmd.Parameters.AddWithValue("@TestQuestionId", questionId);
                    cmd.Parameters.AddWithValue("@TestDifficultyId", difficultyId);
                    cmd.Parameters.AddWithValue("@TestQuestion", ((question == null) ? DBNull.Value.ToString() : question));
                    cmd.Parameters.AddWithValue("@TestQuestionImagePath", ((questionImagepath == null)? DBNull.Value.ToString() : questionImagepath));
                    cmd.Parameters.AddWithValue("@TestCorrectAnswerId", correctAnswerId);
                    cmd.Parameters.AddWithValue("@CorrectAnswerDescription", ((CorrectAnswerDescription == null) ? DBNull.Value.ToString() : CorrectAnswerDescription));
                    cmd.Parameters.AddWithValue("@CorrectAnswerDescriptionPath", ((CorrectAnswerDescriptionpath == null) ? DBNull.Value.ToString() : CorrectAnswerDescriptionpath));
                    cmd.Parameters.AddWithValue("@TVPQuestionAnswers", answers);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    return true;
                }
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        public bool UpdateQuestions(int testQuestionId, int subjectId, int topicId, int questionId, int difficultyId, string question, string questionImagepath, int correctAnswerId, DataTable answers, string CorrectAnswerDescription, string CorrectAnswerDescriptionpath)
        {
            string connstring = ConfigurationManager.ConnectionStrings["PrepConnectionString"].ConnectionString;
            try
            {
                using (SqlConnection conn = new SqlConnection(connstring))
                {
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandText = "spUpdateQuestions"; //The name of the other stored procedure.  
                    cmd.Parameters.AddWithValue("@TestQId", testQuestionId);
                    cmd.Parameters.AddWithValue("@TestQuestionsSubjectId", subjectId);
                    cmd.Parameters.AddWithValue("@TestTopicsId", topicId);
                    cmd.Parameters.AddWithValue("@TestQuestionId", questionId);
                    cmd.Parameters.AddWithValue("@TestDifficultyId", difficultyId);
                    cmd.Parameters.AddWithValue("@TestQuestion", ((question == null) ? DBNull.Value.ToString() : question));
                    cmd.Parameters.AddWithValue("@TestQuestionImagePath", ((questionImagepath == null) ? DBNull.Value.ToString() : questionImagepath));
                    cmd.Parameters.AddWithValue("@TestCorrectAnswerId", correctAnswerId);
                    cmd.Parameters.AddWithValue("@CorrectAnswerDescription", ((CorrectAnswerDescription == null) ? DBNull.Value.ToString() : CorrectAnswerDescription));
                    cmd.Parameters.AddWithValue("@CorrectAnswerDescriptionPath", ((CorrectAnswerDescriptionpath == null) ? DBNull.Value.ToString() : CorrectAnswerDescriptionpath));
                    cmd.Parameters.AddWithValue("@TVPQuestionAnswers", answers);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public DataTable CreateAnswersTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("TestAnswerSequence", typeof(Int32));
            dt.Columns.Add("TestAnswer", typeof(String));
            dt.Columns.Add("TestAnswerImage", typeof(String));
            return dt;
        }

        public List<TestQuestionsList> GetListTestQuestions(int PageSize, int Page, ArrayList arrSearch)
        {
            string searchString = "", optionAnswerString="";

            if (arrSearch[0] != null)
            {
                searchString = " and TestQuestionsSubjectId=" + arrSearch[0];
            }
            if (arrSearch[1] != null)
            {
                if (Convert.ToInt32(arrSearch[1]) != 0)
                {
                    searchString += " and TestTopicsId=" + arrSearch[1];
                }
            }
            if (arrSearch[2] != null)
            {
                searchString += " and TestDifficultyId=" + arrSearch[2];
            }

            if (arrSearch[3] != null && arrSearch[3].ToString() != "")
            {
                optionAnswerString += " and TestQuestion like '%" + arrSearch[3] + "%'";
            }

            DataSet ds = new DataSet();
            string connstring = ConfigurationManager.ConnectionStrings["PrepConnectionString"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connstring))
            {
                SqlCommand sqlComm = new SqlCommand("spGetActiveQuestionList", conn);

                sqlComm.CommandType = CommandType.StoredProcedure;
                sqlComm.Parameters.AddWithValue("@PageSize", PageSize);
                sqlComm.Parameters.AddWithValue("@Page", Page);
                sqlComm.Parameters.AddWithValue("@SearchString", searchString);
                sqlComm.Parameters.AddWithValue("@OptionString", optionAnswerString);

                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = sqlComm;

                da.Fill(ds);
                var questionSubjectList = ds.Tables[0].AsEnumerable().Select(dataRow => new TestQuestionsList
                {
                    TestQuestionId = dataRow.Field<int>("TestQuestionsId")
                    , TestQuestionSubjects = dataRow.Field<string>("SubjectName")
                    , TestTopic = dataRow.Field<string>("TopicName")
                    , TestDifficulty = dataRow.Field<string>("DiffLevelValue")
                    , TestQuestionTypeId = dataRow.Field<string>("QuestionType")
                    , TestQuestion = dataRow.Field<string>("TestQuestion")
                    , TotalRecordCount = dataRow.Field<int>("TotalRecordCount")
                    , TestQuestionsSubjectId = dataRow.Field<int>("TestQuestionsSubjectId")
                    , TestTopicsId = dataRow.Field<int>("TestTopicsId")
                    , TestDifficultyId = dataRow.Field<int>("TestDifficultyId")
                    , CorrectAnswer = dataRow.Field<string>("CorrectAnswer")

                }).ToList();

                return questionSubjectList;
            }
        }

        public bool DeleteTestQuestions(int TestQuestionsId)
        {
            DataSet ds = new DataSet();
            string connstring = ConfigurationManager.ConnectionStrings["PrepConnectionString"].ConnectionString;
            try
            {
                using (SqlConnection conn = new SqlConnection(connstring))
                {
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandText = "spDeleteQuestion"; //The name of the other stored procedure.  
                    cmd.Parameters.AddWithValue("@TestQuestionId", TestQuestionsId);

                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        
        public bool DeleteSubject(int TestQuestionsSubjectId)
        {
            DataSet ds = new DataSet();
            string connstring = ConfigurationManager.ConnectionStrings["PrepConnectionString"].ConnectionString;
            try
            {
                using (SqlConnection conn = new SqlConnection(connstring))
                {
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandText = "spDeleteSubject"; //The name of the other stored procedure.  
                    cmd.Parameters.AddWithValue("@TestQuestionSubjectId", TestQuestionsSubjectId);

                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public DataSet GetTestQuestionById(int TestQuestionsId)
        {
            DataSet ds = new DataSet();
            string connstring = ConfigurationManager.ConnectionStrings["PrepConnectionString"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connstring))
            {
                SqlCommand sqlComm = new SqlCommand("spGetTestQuestionById", conn);
                sqlComm.Parameters.Add(new SqlParameter("@QuestionId", TestQuestionsId));

                sqlComm.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = sqlComm;

                da.Fill(ds);
                return ds;
            }

        }

        public bool AddSubjects(string SubjectName)
        {
            string connstring = ConfigurationManager.ConnectionStrings["PrepConnectionString"].ConnectionString;
            try
            {
                using (SqlConnection conn = new SqlConnection(connstring))
                {
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandText = "spAddSubjects"; //The name of the other stored procedure.  
                    cmd.Parameters.AddWithValue("@SubjectName", SubjectName);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        
        public bool UpdateSubjects(int SubjectId, string SubjectName)
        {
            string connstring = ConfigurationManager.ConnectionStrings["PrepConnectionString"].ConnectionString;
            try
            {
                using (SqlConnection conn = new SqlConnection(connstring))
                {
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandText = "spUpdateSubjects"; //The name of the other stored procedure.  
                    cmd.Parameters.AddWithValue("@SubjectId", SubjectId);
                    cmd.Parameters.AddWithValue("@SubjectName", SubjectName);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        
        //public bool DeleteSubject(int TestQuestionsSubjectId)
        //{
        //    DataSet ds = new DataSet();
        //    string connstring = ConfigurationManager.ConnectionStrings["PrepConnectionString"].ConnectionString;
        //    try
        //    {
        //        using (SqlConnection conn = new SqlConnection(connstring))
        //        {
        //            SqlCommand cmd = conn.CreateCommand();
        //            cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //            cmd.CommandText = "spDeleteSubject"; //The name of the other stored procedure.  
        //            cmd.Parameters.AddWithValue("@TestQuestionSubjectId", TestQuestionsSubjectId);

        //            conn.Open();
        //            cmd.ExecuteNonQuery();
        //            conn.Close();
        //            return true;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //    }
        //}
        public DataSet GetSubjectByName(string SubjectName, int SubjectId)
        {
            DataSet ds = new DataSet();
            string connstring = ConfigurationManager.ConnectionStrings["PrepConnectionString"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connstring))
            {
                SqlCommand sqlComm = new SqlCommand("spGetSubjectByName", conn);
                sqlComm.Parameters.Add(new SqlParameter("@SubjectName", SubjectName));
                sqlComm.Parameters.Add(new SqlParameter("@SubjectId", SubjectId));

                sqlComm.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = sqlComm;

                da.Fill(ds);
                return ds;
            }

        }

        public DataSet GetDiffLevelByName(string DiffLevelName, int DiffLevelNameId)
        {
            DataSet ds = new DataSet();
            string connstring = ConfigurationManager.ConnectionStrings["PrepConnectionString"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connstring))
            {
                SqlCommand sqlComm = new SqlCommand("spGetDifficultyLevelByName", conn);
                sqlComm.Parameters.Add(new SqlParameter("@DiffLevelName", DiffLevelName));
                sqlComm.Parameters.Add(new SqlParameter("@DiffLevelId", DiffLevelNameId));

                sqlComm.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = sqlComm;

                da.Fill(ds);
                return ds;
            }

        }

        public bool DeleteDifficultyLevel(int DifficultyLevelId)
        {
            DataSet ds = new DataSet();
            string connstring = ConfigurationManager.ConnectionStrings["PrepConnectionString"].ConnectionString;
            try
            {
                using (SqlConnection conn = new SqlConnection(connstring))
                {
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandText = "spDeleteDifficultyLevel"; //The name of the other stored procedure.  
                    cmd.Parameters.AddWithValue("@DiffLevelId", DifficultyLevelId);

                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        
        public bool AddDifficultyLevel(string DifficultyLevelName)
        {
            string connstring = ConfigurationManager.ConnectionStrings["PrepConnectionString"].ConnectionString;
            try
            {
                using (SqlConnection conn = new SqlConnection(connstring))
                {
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandText = "spAddDifficulty"; //The name of the other stored procedure.  
                    cmd.Parameters.AddWithValue("@DiffLevelValue", DifficultyLevelName);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        
        public bool UpdateDifficultyLevel(int DifficultyLevelId, string DifficultyLevelName)
        {
            string connstring = ConfigurationManager.ConnectionStrings["PrepConnectionString"].ConnectionString;
            try
            {
                using (SqlConnection conn = new SqlConnection(connstring))
                {
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandText = "spUpdateDifficulty"; //The name of the other stored procedure.  
                    cmd.Parameters.AddWithValue("@DiffLevelId", DifficultyLevelId);
                    cmd.Parameters.AddWithValue("@DiffLevelValue", DifficultyLevelName);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public List<TestSubjectToic> GetActiveTopicList()
        {
            string connstring = ConfigurationManager.ConnectionStrings["PrepConnectionString"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connstring))
            {
                DataSet ds = new DataSet();
                SqlCommand sqlComm = new SqlCommand("spGetActiveTopicList", conn);

                sqlComm.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = sqlComm;

                da.Fill(ds);
                var testSubjectToicList = ds.Tables[0].AsEnumerable().Select(dataRow => new TestSubjectToic
                {
                    TopicId = dataRow.Field<int>("TopicId")
                    ,
                    SubjectName = dataRow.Field<string>("SubjectName")
                    ,
                    TopicName = dataRow.Field<string>("TopicName")
                    ,
                    TopicQuestionsCount = dataRow.Field<int>("TopicQuestionsCount")

                }).ToList();

                return testSubjectToicList;
            }
        }
        
        public bool DeleteTopic(int TopicId)
        {
            DataSet ds = new DataSet();
            string connstring = ConfigurationManager.ConnectionStrings["PrepConnectionString"].ConnectionString;
            try
            {
                using (SqlConnection conn = new SqlConnection(connstring))
                {
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandText = "spDeleteTopic"; //The name of the other stored procedure.  
                    cmd.Parameters.AddWithValue("@TopicId", TopicId);

                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public DataSet GetTopicByName(string TopicName, int TopicId)
        {
            DataSet ds = new DataSet();
            string connstring = ConfigurationManager.ConnectionStrings["PrepConnectionString"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connstring))
            {
                SqlCommand sqlComm = new SqlCommand("spGetTopicByName", conn);
                sqlComm.Parameters.Add(new SqlParameter("@TopicName", TopicName));
                sqlComm.Parameters.Add(new SqlParameter("@TopicId", TopicId));

                sqlComm.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = sqlComm;

                da.Fill(ds);
                return ds;
            }

        }


        public bool AddTopics(int SubjectId,string TopicName)
        {
            string connstring = ConfigurationManager.ConnectionStrings["PrepConnectionString"].ConnectionString;
            try
            {
                using (SqlConnection conn = new SqlConnection(connstring))
                {
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandText = "spAddTopics"; //The name of the other stored procedure.  
                    cmd.Parameters.AddWithValue("@SubjectId", SubjectId);
                    cmd.Parameters.AddWithValue("@TopicName", TopicName);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool UpdateTopics(int TopicId, int SubjectId, string TopicName)
        {
            string connstring = ConfigurationManager.ConnectionStrings["PrepConnectionString"].ConnectionString;
            try
            {
                using (SqlConnection conn = new SqlConnection(connstring))
                {
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandText = "spUpdateTopics"; //The name of the other stored procedure.  
                    cmd.Parameters.AddWithValue("@TopicId", TopicId);
                    cmd.Parameters.AddWithValue("@SubjectId", SubjectId);
                    cmd.Parameters.AddWithValue("@TopicName", TopicName);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public DataSet GetQuestionBySubjectId(int SubjectId)
        {
            DataSet ds = new DataSet();
            string connstring = ConfigurationManager.ConnectionStrings["PrepConnectionString"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connstring))
            {
                SqlCommand sqlComm = new SqlCommand("spGetQuestionsBySubject", conn);
                sqlComm.Parameters.Add(new SqlParameter("@SubjectId", SubjectId));

                sqlComm.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = sqlComm;

                da.Fill(ds);
                return ds;
            }
        }
    }
}