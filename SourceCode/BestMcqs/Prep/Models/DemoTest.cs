﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Prep.Models
{
    public class DemoTest
    {
        public Int64 RowNo { get; set; }
        public int TestQuestionsId { get; set; }
        public int TestQuestionsSubjectId { get; set; }
        public int TestTopicsId { get; set; }
        public string TestQuestion { get; set; }
        public int TestQuestionSeqId { get; set; }
        public string TestQuestionAnswer { get; set; }
        public string TestAnswerImage { get; set; }
        public string CorrectAnswerDescription { get; set; }
        public string TestQuestionImage { get; set; }
        public string CorrectAnswerDescriptionImage { get; set; }
    }
}