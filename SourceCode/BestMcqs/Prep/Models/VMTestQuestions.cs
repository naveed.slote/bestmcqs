﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Prep.Models
{
    public class VMTestQuestions
    {
        [DisplayName("Subjects")]
        public List<TestQuestionsSubjects> TestQuestionSubjects { get; set; }

        [Required(ErrorMessage = "Please select Subject")]
        public int SelectedTestQuestionSubjectId { set; get; }
        [DisplayName("Topics")]
        public List<TestTopics> TestTopic { get; set; }
        [Required(ErrorMessage = "Please select Topic")]
        public int SelectedTestTopicId { set; get; }
        [DisplayName("Difficulty")]
        public List<TestDifficulties> TestDifficulty { get; set; }
        [Required(ErrorMessage = "Please select Difficulty Level")]
        public int SelectedTestDifficultyId { set; get; }
        [DisplayName("Question Type")]
        public List<TestQuestionTypes> TestQuestionTypeId { get; set; }
        [Required(ErrorMessage = "Please select Question Type")]
        public int SelectedTestQuestionTypeId { set; get; }
        
        [DisplayName("Questions")]
        //[Required(ErrorMessage = "Please enter test question")]
        public string TestQuestion { get; set; }
        
        public HttpPostedFileBase TestQuestionFile { get; set; }

        [DisplayName("Options")]
        public List<TestQuestionAnswers> TestQuestionOptions { get; set; }

        //public List<TestQuestionAnswerFiles> OptionFile { get; set; }
        //public HttpPostedFileBase OptionFile { get; set; }
        public Boolean TestCorrectAnswerId { get; set; }

        public int TestAnswerId { get; set; }

        [DisplayName("Correct Answer Description")]
        //[Required(ErrorMessage = "Please enter correct answer description")]
        public string CorrectAnswerDescription { get; set; }

        public HttpPostedFileBase AnswerDescriptionFile { get; set; }

        public string TestQuestionOption { get; set; }
        public string TestQuestionFileName { get; set; }
        public string AnswerDescriptionFileName { get; set; }

        public string TestQuestionOptionsFile { get; set; }
        //public List<TestCorrectAnswerIds> TestCorrectAnswerId { get; set; }
    }

    public class TestQuestionTypes
    {
        public int TestQuestionTypeId { get; set; }
        public string TestQuestionTypeName { get; set; }
    }
    public class TestDifficulties
    {
        public int TestDifficultyId { get; set; }
        public string TestDifficultyName { get; set; }
    }
    public class TestTopics
    {
        public int TestTopicsId { get; set; }
        public string TestTopicsName { get; set; }
    }
    public class TestQuestionsSubjects
    {
        public int TestQuestionsSubjectId { get; set; }
        public string TestQuestionsSubject { get; set; }
        public int TestSubQuestionCount { get; set; }
    }
    public class TestQuestionAnswers
    {
        public int TestQuestionId { get; set; }
        public string TestQuestionAnswer { get; set; }
    }
    


    public class TestQuestionsList
    {
        public int TestQuestionId { get; set; }
        public string TestQuestionSubjects { get; set; }
        public string TestTopic { get; set; }
        public string TestDifficulty { get; set; }
        public string TestQuestionTypeId { get; set; }
        public string TestQuestion { get; set; }
        public int TotalRecordCount { get; set; }

        public int? TestQuestionsSubjectId { get; set; }

        public int? TestTopicsId { get; set; }

        public int? TestDifficultyId { get; set; }

        public string CorrectAnswer { get; set; }

    }
    public class TestQuestionModel
    {
        public List<TestQuestionsList> TestQuestions { get; set; }

        ///<summary>
        /// Gets or sets CurrentPageIndex.
        ///</summary>
        public int CurrentPageIndex { get; set; }

        ///<summary>
        /// Gets or sets PageCount.
        ///</summary>
        public int PageCount { get; set; }

        public int TotalRecords { get; set; }
    }
    public class TestQuestionSearchModel
    {
        [DisplayName("Subjects")]
        public List<TestQuestionsSubjects> TestQuestionSubjects { get; set; }

        public int? SelectedTestQuestionSubjectId { set; get; }
        [DisplayName("Topics")]
        public List<TestTopics> TestTopic { get; set; }
        public int? SelectedTestTopicId { set; get; }
        [DisplayName("Difficulty")]
        public List<TestDifficulties> TestDifficulty { get; set; }
        public int? SelectedTestDifficultyId { set; get; }
    }

    public class TestSubjectToic
    {
        public int TopicId { get; set; }
        public string SubjectName { get; set; }
        public string TopicName { get; set; }
        public int TopicQuestionsCount { get; set; }
    }


    public class VMTestTopics
    {
        [DisplayName("Subjects")]
        public List<TestQuestionsSubjects> TestQuestionSubjects { get; set; }

        [Required(ErrorMessage = "Please select Subject")]
        public int SelectedTestQuestionSubjectId { set; get; }

        public string TopicName { get; set; }
    }
}