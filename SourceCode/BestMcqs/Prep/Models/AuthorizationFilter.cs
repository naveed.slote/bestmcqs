﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Filters;

namespace Prep.Models
{
    public class AuthorizationFilter : AuthorizeAttribute, IAuthorizationFilter
    {
        public override void OnAuthorization(AuthorizationContext filterContext)//https://www.c-sharpcorner.com/blogs/applying-authorization-using-session-in-asp-net-mvc
        {
            if (filterContext.ActionDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true)
                || filterContext.ActionDescriptor.ControllerDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true))
            {
                // Don't check for authorization as AllowAnonymous filter is applied to the action or controller  
                return;
            }

            // Check for authorization  
            if (HttpContext.Current.Session["LoginUserId"] == null)
            {
                filterContext.Result = new RedirectResult("~/Admin/Login");
            }
        }
    }

    //public class CustomAuthenticationAttribute : ActionFilterAttribute
    //{
    //    public void OnAuthentication(AuthenticationContext filterContext)
    //    {

    //        //For demo purpose only. In real life your custom principal might be retrieved via different source. i.e context/request etc.
    //        if (HttpContext.Current.Session["LoginUserId"] != null)
    //            return;
    //    }
        
    //}
}