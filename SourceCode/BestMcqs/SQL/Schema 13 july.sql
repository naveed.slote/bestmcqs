USE [PrepApp]
GO
/****** Object:  UserDefinedTableType [dbo].[TestAnswers]    Script Date: 7/13/2018 3:29:32 PM ******/
CREATE TYPE [dbo].[TestAnswers] AS TABLE(
	[TestAnswerId] [int] NULL,
	[TestAnswer] [nvarchar](max) NULL,
	[TestAnswerImage] [nvarchar](max) NULL,
	[TestAnswerImageTag] [nvarchar](max) NULL
)
GO
/****** Object:  UserDefinedFunction [dbo].[CSVToTable]    Script Date: 7/13/2018 3:29:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[CSVToTable] (@InStr VARCHAR(MAX))
RETURNS @TempTab TABLE
   (id int not null)
AS
BEGIN
    ;-- Ensure input ends with comma
	SET @InStr = REPLACE(@InStr + ',', ',,', ',')
	DECLARE @SP INT
DECLARE @VALUE VARCHAR(1000)
WHILE PATINDEX('%,%', @INSTR ) <> 0 
BEGIN
   SELECT  @SP = PATINDEX('%,%',@INSTR)
   SELECT  @VALUE = LEFT(@INSTR , @SP - 1)
   SELECT  @INSTR = STUFF(@INSTR, 1, @SP, '')
   INSERT INTO @TempTab(id) VALUES (@VALUE)
END
	RETURN
END
GO
/****** Object:  Table [dbo].[DifficultyLevel]    Script Date: 7/13/2018 3:29:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DifficultyLevel](
	[DiffLevelId] [int] IDENTITY(1,1) NOT NULL,
	[DiffLevelValue] [nvarchar](500) NULL,
	[isActive] [bit] NULL,
 CONSTRAINT [PK_DifficultyLevel] PRIMARY KEY CLUSTERED 
(
	[DiffLevelId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[QuestionTypes]    Script Date: 7/13/2018 3:29:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QuestionTypes](
	[QuestionTypeId] [int] IDENTITY(1,1) NOT NULL,
	[QuestionType] [nvarchar](500) NULL,
	[isActive] [bit] NULL,
 CONSTRAINT [PK_QuestionTypes] PRIMARY KEY CLUSTERED 
(
	[QuestionTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Subjects]    Script Date: 7/13/2018 3:29:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Subjects](
	[SubjectId] [int] IDENTITY(1,1) NOT NULL,
	[SubjectName] [nvarchar](500) NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_Subjects] PRIMARY KEY CLUSTERED 
(
	[SubjectId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TestQuestionAnswers]    Script Date: 7/13/2018 3:29:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TestQuestionAnswers](
	[TestQuestionsAnswerId] [int] IDENTITY(1,1) NOT NULL,
	[TestQuestionsId] [int] NOT NULL,
	[TestQuestionSeqId] [int] NOT NULL,
	[TestQuestionAnswer] [nvarchar](max) NOT NULL,
	[TestAnswerImage] [nvarchar](max) NULL,
	[TestAnswerImageTag] [varchar](50) NULL,
 CONSTRAINT [PK_TestQuestionAnswers] PRIMARY KEY CLUSTERED 
(
	[TestQuestionsAnswerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TestQuestions]    Script Date: 7/13/2018 3:29:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TestQuestions](
	[TestQuestionsId] [int] IDENTITY(1,1) NOT NULL,
	[TestQuestionsSubjectId] [int] NOT NULL,
	[TestTopicsId] [int] NOT NULL,
	[TestQuestionId] [int] NOT NULL,
	[TestDifficultyId] [int] NOT NULL,
	[TestQuestion] [nvarchar](max) NOT NULL,
	[TestCorrectAnswerId] [int] NOT NULL,
	[isActive] [bit] NOT NULL,
	[CorrectAnswerDescription] [nvarchar](max) NULL,
	[TestQuestionImage] [nvarchar](max) NULL,
	[CorrectAnswerDescriptionImage] [nvarchar](max) NULL,
	[TestQuestionImageTag] [varchar](50) NULL,
 CONSTRAINT [PK_TestQuestions] PRIMARY KEY CLUSTERED 
(
	[TestQuestionsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Topics]    Script Date: 7/13/2018 3:29:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Topics](
	[TopicId] [int] IDENTITY(1,1) NOT NULL,
	[TopicName] [nvarchar](500) NULL,
	[SubjectId] [int] NOT NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_Topics] PRIMARY KEY CLUSTERED 
(
	[TopicId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[spAddDifficulty]    Script Date: 7/13/2018 3:29:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spAddDifficulty] 
	@DiffLevelValue nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [dbo].[DifficultyLevel]
           ([DiffLevelValue]
           ,[isActive])
     VALUES
           (@DiffLevelValue
           ,1)


END

GO
/****** Object:  StoredProcedure [dbo].[spAddQuestions]    Script Date: 7/13/2018 3:29:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spAddQuestions] 
	-- Add the parameters for the stored procedure here
	@TestQuestionsSubjectId int
    ,@TestTopicsId int
    ,@TestQuestionId int
    ,@TestDifficultyId int
    ,@TestQuestion nvarchar(max)
	,@TestQuestionImagePath nvarchar(max)
	,@TestQuestionImageTag nvarchar(max)
    ,@TestCorrectAnswerId int
	,@CorrectAnswerDescription nvarchar(max)
	,@CorrectAnswerDescriptionPath nvarchar(max)
	,@TVPQuestionAnswers TestAnswers ReadOnly
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
INSERT INTO [dbo].[TestQuestions]
           ([TestQuestionsSubjectId]
           ,[TestTopicsId]
           ,[TestQuestionId]
           ,[TestDifficultyId]
           ,[TestQuestion]
		   ,[TestQuestionImage]
           ,[TestCorrectAnswerId]
           ,[isActive]
		   ,[CorrectAnswerDescription]
		   ,[CorrectAnswerDescriptionImage]
		   ,[TestQuestionImageTag])
     VALUES
           (@TestQuestionsSubjectId
           ,@TestTopicsId
           ,@TestQuestionId
           ,@TestDifficultyId
           ,@TestQuestion
		   ,@TestQuestionImagePath
           ,@TestCorrectAnswerId
           ,1
		   ,@CorrectAnswerDescription
		   ,@CorrectAnswerDescriptionPath
		   ,@TestQuestionImageTag)


		DECLARE @TestQuestionsId int = SCOPE_IDENTITY()



	INSERT INTO [dbo].[TestQuestionAnswers]
           ([TestQuestionsId]
		   ,[TestQuestionSeqId]
           ,[TestQuestionAnswer]
		   ,[TestAnswerImage]
		   ,[TestAnswerImageTag])
           (Select @TestQuestionsId ,TestAnswerId,TestAnswer,TestAnswerImage,TestAnswerImageTag from @TVPQuestionAnswers)


END

GO
/****** Object:  StoredProcedure [dbo].[spAddSubjects]    Script Date: 7/13/2018 3:29:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spAddSubjects]
	@SubjectName nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
	INSERT INTO [dbo].[Subjects]
           ([SubjectName]
		   ,[IsActive])
     VALUES
           (@SubjectName
		   ,1)
END

GO
/****** Object:  StoredProcedure [dbo].[spAddTopics]    Script Date: 7/13/2018 3:29:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spAddTopics] 
	@SubjectId int,
	@TopicName nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

INSERT INTO [dbo].[Topics]
           ([TopicName]
           ,[SubjectId]
           ,[IsActive])
     VALUES
           (@TopicName
           ,@SubjectId
           ,1)


END

GO
/****** Object:  StoredProcedure [dbo].[spDeleteDifficultyLevel]    Script Date: 7/13/2018 3:29:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spDeleteDifficultyLevel] 
	@DiffLevelId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	UPDATE [dbo].[DifficultyLevel]
	SET [isActive] = 0
	WHERE DiffLevelId=@DiffLevelId


END

GO
/****** Object:  StoredProcedure [dbo].[spDeleteQuestion]    Script Date: 7/13/2018 3:29:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spDeleteQuestion]
	@TestQuestionId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Update TestQuestions
	Set isActive=0
	where TestQuestionsId=@TestQuestionId
    
END

GO
/****** Object:  StoredProcedure [dbo].[spDeleteSubject]    Script Date: 7/13/2018 3:29:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spDeleteSubject] 
	@TestQuestionSubjectId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;    

		UPDATE [dbo].[Subjects]
		   SET [IsActive] = 0
		 WHERE SubjectId=@TestQuestionSubjectId 
END

GO
/****** Object:  StoredProcedure [dbo].[spDeleteTopic]    Script Date: 7/13/2018 3:29:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spDeleteTopic] 
	@TopicId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE [dbo].[Topics]
      SET [IsActive] = 0
	WHERE TopicId=@TopicId


END

GO
/****** Object:  StoredProcedure [dbo].[spGetActiveDifficultyLevel]    Script Date: 7/13/2018 3:29:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetActiveDifficultyLevel] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	SELECT [DiffLevelId]
      ,[DiffLevelValue]
  FROM [dbo].[DifficultyLevel]
  where isActive=1
  Order By DiffLevelValue

END

GO
/****** Object:  StoredProcedure [dbo].[spGetActiveQuestionList]    Script Date: 7/13/2018 3:29:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetActiveQuestionList] 
	
    @PageSize INT,
    @Page INT,
	@SearchString NVARCHAR(1000),
	@OptionString NVARCHAR(1000)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @SQL NVARCHAR(MAX)

SET @SQL = ';WITH PageNumbers AS( '+
			'SELECT *, '+
			'		ROW_NUMBER() OVER(ORDER BY TestQuestionsId) ID '+
			'FROM    TestQuestions '+
			'where isActive=1 '

	
	IF(@SearchString <> '')
	BEGIN
		
	--	IF(@SearchString like '% and TestQuestion like ''%''%')
	--	BEGIN
	--		SET @SearchString = replace(@SearchString,' and TestQuestion like ''%''','')
	--	END
	--	Print @SearchString
		
		SET @SQL = @SQL + '' + @SearchString + ''
	--	Print @SQL
	--	SET @SearchString = replace(@SearchString,' and TestQuestion like','')
		
	--	SET @SQL = @SQL + ' OR TestQuestionsId IN (Select TestQuestionsId from TestQuestionAnswers where TestQuestionAnswer like '+@SearchString+') '
	END

	if(@OptionString <> '')
	BEGIN
		SET @OptionString = replace(@OptionString,' and TestQuestion like','')
		Print @OptionString
		SET @SQL = @SQL + ' AND TestQuestionsId IN (Select TestQuestionsId from TestQuestionAnswers where TestQuestionAnswer like '+@OptionString+') AND TestQuestion  like  '+@OptionString+' '
		Print @SQL
	END

	SET @SQL = @SQL + ')'




	SET @SQL = @SQL + 'SELECT PageNumbers.TestQuestionsId, Subjects.SubjectName,Topics.TopicName,DifficultyLevel.DiffLevelValue,QuestionTypes.QuestionType,PageNumbers.TestQuestion,(Select Count(TestQuestionsId) from PageNumbers) TotalRecordCount '+
	' ,TestQuestionsSubjectId,TestTopicsId,TestDifficultyId '+
	',STUFF((SELECT '', '' + TestQuestionAnswer '+
    '     FROM TestQuestionAnswers '+
    '     WHERE TestQuestionAnswers.TestQuestionsId = PageNumbers.TestQuestionsId '+
    '     FOR XML PATH(''''), TYPE) '+
    '    .value(''.'',''NVARCHAR(MAX)''),1,2,'' '') CorrectAnswer '+
	'FROM    PageNumbers '+
	'inner join Subjects on PageNumbers.TestQuestionsSubjectId = Subjects.SubjectId '+
	'inner join Topics on PageNumbers.TestTopicsId = Topics.TopicId '+
	'inner join DifficultyLevel on PageNumbers.TestDifficultyId = DifficultyLevel.DiffLevelId '+
	'inner join QuestionTypes on PageNumbers.TestQuestionId = QuestionTypes.QuestionTypeId '+
	'WHERE   ID  BETWEEN (('+Convert(Varchar,@Page)+' - 1) * '+Convert(Varchar,@PageSize)+' + 1) '+
			'AND ('+Convert(Varchar,@Page)+' * '+Convert(Varchar,@PageSize)+') ';

	
	if(@OptionString <> '')
	BEGIN
		SET @OptionString = replace(@OptionString,' and TestQuestion like','')
		Print @OptionString
		--SET @SQL = @SQL + ' AND TestQuestionsId IN (Select TestQuestionsId from TestQuestionAnswers where TestQuestionAnswer like '+@OptionString+') OR TestQuestion  like  '+@OptionString+' '
		SET @SQL = @SQL + ' AND PageNumbers.TestQuestion like '+@OptionString+' and PageNumbers.TestQuestionId IN (Select TestQuestionId from TestQuestionAnswers where TestQuestionAnswer like '+@OptionString+') '
		Print @SQL
	END

	Print @SQL
	exec(@SQL)
END 
GO
/****** Object:  StoredProcedure [dbo].[spGetActiveQuestionList_New]    Script Date: 7/13/2018 3:29:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetActiveQuestionList_New] 
	
    @PageSize INT,
    @Page INT,
	@SearchString NVARCHAR(1000),
	@OptionString NVARCHAR(1000),
	@OptionStringForImage NVARCHAR(1000)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @SQL NVARCHAR(MAX)

SET @SQL = ';WITH PageNumbers AS( '+
			'SELECT *, '+
			'		ROW_NUMBER() OVER(ORDER BY TestQuestionsId) ID '+
			'FROM    TestQuestions '+
			'where isActive=1 '

	
	IF(@SearchString <> '')
	BEGIN
		SET @SQL = @SQL + '' + @SearchString + ''
	END

	if(@OptionString <> '')
	BEGIN
		SET @OptionString = replace(@OptionString,' and TestQuestion like','')
		Print @OptionString
		SET @SQL = @SQL + ' AND TestQuestionsId IN (Select TestQuestionsId from TestQuestionAnswers where TestQuestionAnswer like '+@OptionString+') AND TestQuestion  like  '+@OptionString+' '
		Print @SQL
	END

	if(@OptionStringForImage <> '')
	BEGIN
		SET @OptionString = replace(@OptionStringForImage,' and TestQuestionImageTag like','')
		Print @OptionString
		SET @SQL = @SQL + ' AND TestQuestionsId IN (Select TestQuestionsId from TestQuestionAnswers where TestQuestionAnswer like '+@OptionStringForImage+') AND TestQuestionImageTag  like  '+@OptionStringForImage+' '
		Print @SQL
	END

	SET @SQL = @SQL + ')'
	SET @SQL = @SQL + 'SELECT PageNumbers.TestQuestionsId, Subjects.SubjectName,Topics.TopicName,DifficultyLevel.DiffLevelValue,QuestionTypes.QuestionType,PageNumbers.TestQuestion,(Select Count(TestQuestionsId) from PageNumbers) TotalRecordCount '+
	' ,TestQuestionsSubjectId,TestTopicsId,TestDifficultyId '+
	',STUFF((SELECT '', '' + TestQuestionAnswer '+
    '     FROM TestQuestionAnswers '+
    '     WHERE TestQuestionAnswers.TestQuestionsId = PageNumbers.TestQuestionsId '+
    '     FOR XML PATH(''''), TYPE) '+
    '    .value(''.'',''NVARCHAR(MAX)''),1,2,'' '') CorrectAnswer '+
	'FROM    PageNumbers '+
	'inner join Subjects on PageNumbers.TestQuestionsSubjectId = Subjects.SubjectId '+
	'inner join Topics on PageNumbers.TestTopicsId = Topics.TopicId '+
	'inner join DifficultyLevel on PageNumbers.TestDifficultyId = DifficultyLevel.DiffLevelId '+
	'inner join QuestionTypes on PageNumbers.TestQuestionId = QuestionTypes.QuestionTypeId '+
	'WHERE   ID  BETWEEN (('+Convert(Varchar,@Page)+' - 1) * '+Convert(Varchar,@PageSize)+' + 1) '+
			'AND ('+Convert(Varchar,@Page)+' * '+Convert(Varchar,@PageSize)+') ';
	if(@OptionString <> '')
	BEGIN
		SET @OptionString = replace(@OptionString,' and TestQuestion like','')
		Print @OptionString
		SET @SQL = @SQL + ' AND PageNumbers.TestQuestion like '+@OptionString+' and PageNumbers.TestQuestionId IN (Select TestQuestionId from TestQuestionAnswers where TestQuestionAnswer like '+@OptionString+') '
		Print @SQL
	END
	if(@OptionStringForImage <> '')
	BEGIN
		SET @OptionStringForImage = replace(@OptionStringForImage,' and TestQuestionImageTag like','')
		Print @OptionStringForImage
		SET @SQL = @SQL + ' AND PageNumbers.TestQuestionImageTag like '+@OptionStringForImage+' and PageNumbers.TestQuestionId IN (Select TestQuestionId from TestQuestionAnswers where TestQuestionAnswer like '+@OptionStringForImage+') '
		Print @SQL
	END


	Print @SQL
	exec(@SQL)
END 
GO
/****** Object:  StoredProcedure [dbo].[spGetActiveQuestionList_New1]    Script Date: 7/13/2018 3:29:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetActiveQuestionList_New1] 
	
	--@SearchString NVARCHAR(1000),
	@testQuestionsSubjectId NVARCHAR (50) = 0,
	@testTopicsId NVARCHAR (50) = 0,
	@testDifficultyId NVARCHAR (50) = 0,
	@OptionString NVARCHAR(1000) = 0,
	@OptionStringForImage NVARCHAR(1000) = 0,
	@RowsPerPage INT = 10,
	@PageNumber INT = 1
AS
BEGIN
declare @qry as varchar(MAX)
SET @qry ='
	SELECT  TestQuestions.TestQuestionsId, Subjects.SubjectName ,Topics.TopicName,DifficultyLevel.DiffLevelValue ,
	QuestionTypes.QuestionType,TestQuestions.TestQuestion,
	STUFF((SELECT '','' + TestQuestionAnswer
       FROM TestQuestionAnswers 
         WHERE TestQuestionAnswers.TestQuestionsId = TestQuestions.TestQuestionsId
         FOR XML PATH(''''), TYPE) 
        .value(''.'',''NVARCHAR(MAX)''),1,2,'''') CorrectAnswer,
		TestQuestions.TestQuestionsSubjectId,
		TestQuestions.TestTopicsId,TestQuestions.TestDifficultyId
		,(COUNT(*) over() / '+CAST(@RowsPerPage as varchar)+' ) AS TotalPages
	 FROM TestQuestions
	INNER JOIN Subjects on TestQuestions.TestQuestionsSubjectId = Subjects.SubjectId
	INNER JOIN Topics on TestQuestions.TestTopicsId = Topics.TopicId
	INNER JOIN DifficultyLevel on TestQuestions.TestDifficultyId = DifficultyLevel.DiffLevelId
	INNER JOIN QuestionTypes on TestQuestions.TestQuestionId = QuestionTypes.QuestionTypeId
	WHERE 
	 ('+@testQuestionsSubjectId +' =0 OR TestQuestions.TestQuestionsSubjectId = '+@testQuestionsSubjectId+')
	AND 
	('+@testTopicsId+' =0 or TestQuestions.TestTopicsId ='+@testTopicsId+')
	 AND
	('+@testDifficultyId+' = 0 OR TestQuestions.TestDifficultyId ='+@testDifficultyId+')
	 AND
	('''+@OptionString+''' = ''0'' OR TestQuestions.TestQuestion  LIKE ''%'+@OptionString+'%'')
	AND
	('''+@OptionStringForImage+''' = ''0'' OR TestQuestions.TestQuestionImageTag  LIKE ''%'+@OptionStringForImage+'%'' ) 
	OR  TestQuestions.TestQuestionsId IN (SELECT * FROM dbo.CSVToTable(
	( SELECT top 1
       STUFF((SELECT '', '' + CAST([TestQuestionsId] AS VARCHAR(10)) [text()]
         FROM [TestQuestionAnswers] 
          where (TestAnswerImageTag like ''%'+ @OptionStringForImage+'%'')
         FOR XML PATH(''''), TYPE)
        .value(''.'',''NVARCHAR(MAX)''),1,2,'' '') List_Output
FROM [TestQuestionAnswers] t
GROUP BY [TestQuestionsId])
	))
	ORDER BY TestQuestionsId
	  OFFSET ('+CAST(@PageNumber as varchar)+'-1)*'+CAST(@RowsPerPage as varchar) +' ROWS
     FETCH NEXT '+CAST(@RowsPerPage as varchar)+' ROWS ONLY '
	-- print @qry

	Exec(@qry)
--	print @qry
END 
GO
/****** Object:  StoredProcedure [dbo].[spGetActiveQuestionType]    Script Date: 7/13/2018 3:29:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetActiveQuestionType] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	SELECT [QuestionTypeId]
      ,[QuestionType]
  FROM [dbo].[QuestionTypes]
  where isActive=1


END

GO
/****** Object:  StoredProcedure [dbo].[spGetActiveSubjects]    Script Date: 7/13/2018 3:29:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetActiveSubjects] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT [SubjectId]
      ,[SubjectName]      
	FROM [Subjects]
	where IsActive=1
	Order By SubjectName

END

GO
/****** Object:  StoredProcedure [dbo].[spGetActiveTopicBySubjectId]    Script Date: 7/13/2018 3:29:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetActiveTopicBySubjectId] 
	@SubjectId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	SELECT [TopicId]
      ,[TopicName]
	  FROM [dbo].[Topics]
	where IsActive=1 and SubjectId=@SubjectId
	Order By TopicName

END

GO
/****** Object:  StoredProcedure [dbo].[spGetActiveTopicList]    Script Date: 7/13/2018 3:29:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetActiveTopicList]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    Select TopicId,Subjects.SubjectName,Topics.TopicName from Topics
	inner join Subjects on Topics.SubjectId=Subjects.SubjectId
	where Topics.IsActive=1
END

GO
/****** Object:  StoredProcedure [dbo].[spGetDifficultyLevelByName]    Script Date: 7/13/2018 3:29:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetDifficultyLevelByName] 
	@DiffLevelName nvarchar(MAX),
	@DiffLevelId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    if(@DiffLevelId = 0)
	BEGIN
		Select * from DifficultyLevel
		where DiffLevelValue=@DiffLevelName and isActive=1
	End
	else
	BEGIN
		Select * from DifficultyLevel
		where DiffLevelId=@DiffLevelId and isActive=1

	END
END
GO
/****** Object:  StoredProcedure [dbo].[spGetSubjectByName]    Script Date: 7/13/2018 3:29:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetSubjectByName] 
	@SubjectName nvarchar(MAX),
	@SubjectId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if(@SubjectId = 0)
	BEGIN
		Select * from Subjects
		where SubjectName=@SubjectName and IsActive=1
	End
	else
	BEGIN
		Select * from Subjects
		where SubjectId=@SubjectId and IsActive=1

	END
END
GO
/****** Object:  StoredProcedure [dbo].[spGetTestQuestionById]    Script Date: 7/13/2018 3:29:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetTestQuestionById] 
	@QuestionId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    Select TestQuestionsSubjectId,TestTopicsId,TestQuestionId,TestDifficultyId,TestQuestion,TestCorrectAnswerId,TestQuestionSeqId,TestQuestionAnswer,TestAnswerImage,TestAnswerImageTag,CorrectAnswerDescription,TestQuestionImage,CorrectAnswerDescriptionImage,TestQuestionImageTag from TestQuestions 
	inner join TestQuestionAnswers on TestQuestions.TestQuestionsId = TestQuestionAnswers.TestQuestionsId
	where TestQuestions.TestQuestionsId=@QuestionId
	Order By TestQuestionSeqId
END

GO
/****** Object:  StoredProcedure [dbo].[spGetTestQuestionById_OLD]    Script Date: 7/13/2018 3:29:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetTestQuestionById_OLD] 
	@QuestionId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    Select TestQuestionsSubjectId,TestTopicsId,TestQuestionId,TestDifficultyId,TestQuestion,TestCorrectAnswerId,TestQuestionSeqId,TestQuestionAnswer,TestAnswerImage,CorrectAnswerDescription,TestQuestionImage,CorrectAnswerDescriptionImage,TestQuestionImageTag from TestQuestions 
	inner join TestQuestionAnswers on TestQuestions.TestQuestionsId = TestQuestionAnswers.TestQuestionsId
	where TestQuestions.TestQuestionsId=@QuestionId
	Order By TestQuestionSeqId
END

GO
/****** Object:  StoredProcedure [dbo].[spGetTopicByName]    Script Date: 7/13/2018 3:29:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetTopicByName] 
	@TopicName nvarchar(MAX),
	@TopicId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	
	if(@TopicId = 0)
	BEGIN
		Select * from Topics
		where TopicName=@TopicName and isActive=1
	End
	else
	BEGIN
		Select * from Topics
		where TopicId=@TopicId and isActive=1

	END
END
GO
/****** Object:  StoredProcedure [dbo].[spUpdateDifficulty]    Script Date: 7/13/2018 3:29:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spUpdateDifficulty] 
	@DiffLevelId int,
	@DiffLevelValue nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	UPDATE [dbo].[DifficultyLevel]
   SET [DiffLevelValue] = @DiffLevelValue
	WHERE DiffLevelId=@DiffLevelId


END

GO
/****** Object:  StoredProcedure [dbo].[spUpdateQuestions]    Script Date: 7/13/2018 3:29:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- =============================================
CREATE PROCEDURE [dbo].[spUpdateQuestions]

	@TestQId int
	,@TestQuestionsSubjectId int
    ,@TestTopicsId int
    ,@TestQuestionId int
    ,@TestDifficultyId int
    ,@TestQuestion nvarchar(max)
	,@TestQuestionImagePath nvarchar(max)
    ,@TestCorrectAnswerId int
	,@CorrectAnswerDescription nvarchar(max)
	,@CorrectAnswerDescriptionPath nvarchar(max)
	,@TVPQuestionAnswers TestAnswers ReadOnly
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	BEGIN TRAN T1;  

	UPDATE [dbo].[TestQuestions]
	SET [TestQuestionsSubjectId] = @TestQuestionsSubjectId
      ,[TestTopicsId] = @TestTopicsId
      ,[TestQuestionId] = @TestQuestionId
      ,[TestDifficultyId] = @TestDifficultyId
      ,[TestQuestion] = @TestQuestion
	  ,[TestQuestionImage] = @TestQuestionImagePath
      ,[TestCorrectAnswerId] = @TestCorrectAnswerId
	  ,[CorrectAnswerDescription] = @CorrectAnswerDescription
	  ,[CorrectAnswerDescriptionImage] = @CorrectAnswerDescriptionPath
	WHERE TestQuestionsId =@TestQId


	 Delete from TestQuestionAnswers
	 where TestQuestionsId=@TestQId

 
	INSERT INTO [dbo].[TestQuestionAnswers]
           ([TestQuestionsId]
		   ,[TestQuestionSeqId]
           ,[TestQuestionAnswer]
		   ,[TestAnswerImage]
		   ,[TestAnswerImageTag]
		   )
           (Select @TestQId ,TestAnswerId,TestAnswer,TestAnswerImage,TestAnswerImageTag from @TVPQuestionAnswers)

	COMMIT TRAN T1;

END

GO
/****** Object:  StoredProcedure [dbo].[spUpdateSubjects]    Script Date: 7/13/2018 3:29:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spUpdateSubjects]
	@SubjectId int,
	@SubjectName nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE [dbo].[Subjects]
	SET [SubjectName] = @SubjectName
    WHERE SubjectId=@SubjectId


END

GO
/****** Object:  StoredProcedure [dbo].[spUpdateTopics]    Script Date: 7/13/2018 3:29:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spUpdateTopics] 
	@TopicId int,
	@SubjectId int,
	@TopicName nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	UPDATE [dbo].[Topics]
   SET [TopicName] = @TopicName
      ,[SubjectId] = @SubjectId
	WHERE TopicId=@TopicId



END

GO
