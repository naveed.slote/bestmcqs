﻿$(document).ready(function () {


    var topicId = getUrlVars()["TopicId"]

    if (topicId != null) {

        GetTopicByTopicId(topicId);
    }
});

function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

function GetTopicByTopicId(topicId) {

    $(document).prop('title', 'Update Topics');
    $("h2").text("Update Topics");

    $.ajax({
        url: 'GetTopicByTopicId?TopicId=' + topicId,
        type: 'GET',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var list = data;
            $('#hdnTestTopicId').val(topicId);
            $('#ddlSubjects').val(list[0].SelectedTestQuestionSubjectId);
            $('#TopicName').val(list[0].TopicName);
            $("input[type=submit]").val('Update');
        },
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
        }
    });
}