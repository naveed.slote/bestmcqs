﻿$(document).ready(function () {


    var subjId = getUrlVars()["SubjectId"]

    if (subjId != null)
        GetSubjectBySubjectId(subjId);

});
function validateForm() {

    if ($('#TestQuestionsSubject').val() == '') {

        alert("Please enter Subject");
        return false;
    }
}

function GetSubjectBySubjectId(subjId) {

    $(document).prop('title', 'Update Subjects');
    $("h2").text("Update Subjects")

    $.ajax({
        url: 'GetSubjectBySubjectId?SubjectId=' + subjId,
        type: 'GET',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var list = data;
            $('#hdnTestSubjectId').val(list[0].TestQuestionsSubjectId);
            $('#TestQuestionsSubject').val(list[0].TestQuestionsSubject);
            $("input[type=submit]").val('Update');
        },
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
        }
    });
}

function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}
